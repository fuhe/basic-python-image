# bashrc file for the images

# set up command line
# PS1='\[\033[01;31m\][\h:\u]\[\033[01;32m\]:\W>\[\033[00m\] '
PS1='\[\e[1;35m\] singularity@\h:\w\n \$ \[\e[m\]'
CLICOLOR=1

export DISPLAY=:0

# add h5ls tab complete
source /etc/_h5ls.sh

# enable tab complete
source /etc/profile.d/bash_completion.sh

# custom commands
alias du='du -h --max-depth=1'
alias la='ls -Alhrt --color'
alias ll='ls -lh --color'
