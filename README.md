Quick Start
===========

   - fork this repo
   - wait for ci to build
   - go to lxplus and run

     ```
     singularity run -B /eos:/eos docker://gitlab-registry.cern.ch/${USER}/basic-python-image:latest
     ```

     (the `-B /eos:/eos` just maps your EOS area to the container, you
     can repeat this call for `/cvmfs` or other FUSE file systems)

   - run whatever scripts you need, you should be able to access
     everything you could normally access on afs.

Adding New Things
=================

You're encouraged to fork this and add whatever you need.

Python Modules
--------------

List these in the `requirements.txt` file

Other programs
--------------

Edit the `Dockerfile` and add a call to `apt-get install -y <package>`

Updates
=================

Just starting to learn how docker works

Thanks
======

Thanks to Matthew Feickert for his [example images][1], most things
are copied from there.

[1]: https://gitlab.cern.ch/aml/containers/docker
